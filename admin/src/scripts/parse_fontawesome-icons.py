# https://stackoverflow.com/questions/36743041/font-awesome-icon-in-select-option

## Gets the select font awesome data from option html tags and rebuils what we need
import os

new_file = ""
with open("select.html") as f:
    for line in f:
        icon = line.split("'")[1]
        icon_hex = line.split(">")[1].split(";")[0]
        new_file += (
            "<option value='fa "
            + icon
            + "' {% if menu_item.icon == 'fa'"
            + icon
            + "' %} selected='selected'{% endif %}>"
            + icon_hex
            + "; "
            + icon
            + "</option>\n"
        )

with open("select_output.html", "w") as f:
    f.writelines(new_file)
