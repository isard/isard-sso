#!flask/bin/python
# coding=utf-8
import json
import logging as log
import os
import sys
import time
import traceback
from uuid import uuid4

from api import app
from flask import (
    Response,
    jsonify,
    redirect,
    render_template,
    request,
    send_from_directory,
    url_for,
)

from ..lib.avatars import Avatars

avatars = Avatars()


@app.route("/avatar/<username>", methods=["GET"])
def avatar(username):
    return send_from_directory(
        os.path.join(app.root_path, "../avatars/master-avatars/"),
        avatars.get_user_avatar(username),
        mimetype="image/jpg",
    )
