#!/usr/bin/env python
# coding=utf-8

import logging as log
import os
import sys
import traceback

from api import app


class loadConfig:
    def __init__(self, app=None):
        try:
            app.config.setdefault("DOMAIN", os.environ["DOMAIN"])

        except Exception as e:
            log.error(traceback.format_exc())
            raise
