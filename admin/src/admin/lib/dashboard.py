import logging as log
import os
import shutil
import traceback
from io import BytesIO
from pprint import pprint

import requests
import yaml
from PIL import Image
from schema import And, Optional, Schema, SchemaError, Use

from admin import app


class Dashboard:
    def __init__(
        self,
    ):
        self.custom_menu = os.path.join(app.root_path, "../custom/menu/custom.yaml")

    def _update_custom_menu(self, custom_menu_part):
        with open(self.custom_menu) as yml:
            menu = yaml.load(yml, Loader=yaml.FullLoader)
        menu = {**menu, **custom_menu_part}
        with open(self.custom_menu, "w") as yml:
            yml.write(yaml.dump(menu, default_flow_style=False))
        return True

    def update_colours(self, colours):
        schema_template = Schema(
            {
                "background": And(Use(str)),
                "primary": And(Use(str)),
                "secondary": And(Use(str)),
            }
        )

        try:
            schema_template.validate(colours)
        except SchemaError:
            return False

        self._update_custom_menu({"colours": colours})
        return self.apply_updates()

    def update_menu(self, menu):
        items = []
        for menu_item in menu.keys():
            for mustexist_key in ["href", "icon", "name", "shortname"]:
                if mustexist_key not in menu[menu_item].keys():
                    return False
            items.append(menu[menu_item])
        self._update_custom_menu({"apps_external": items})
        return self.apply_updates()

    def update_logo(self, logo):
        img = Image.open(logo.stream)
        img.save(os.path.join(app.root_path, "../custom/img/logo.png"))
        img.save(
            os.path.join(
                app.root_path,
                "../custom/system/keycloak/themes/liiibrelite/login/resources/img/logo.png",
            )
        )
        return self.apply_updates()

    def update_background(self, background):
        img = Image.open(background.stream)
        img.save(os.path.join(app.root_path, "../custom/img/background.png"))
        img.save(
            os.path.join(
                app.root_path,
                "../custom/system/keycloak/themes/liiibrelite/login/resources/img/loginBG.png",
            )
        )
        img.save(
            os.path.join(
                app.root_path,
                "../custom/system/keycloak/themes/liiibrelite/login/resources/img/loginBG2.png",
            )
        )
        return self.apply_updates()

    def apply_updates(self):
        resp = requests.get("http://isard-sso-api:7039/restart")
        return True
