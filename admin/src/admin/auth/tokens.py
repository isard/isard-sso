# Copyright 2017 the Isard-vdi project authors:
#      Josep Maria Viñolas Auquer
#      Alberto Larraz Dalmases
# License: AGPLv3

import json
import logging as log
import os
import traceback
from functools import wraps

from flask import request
from jose import jwt

from admin import app

from ..lib.api_exceptions import Error


def get_header_jwt_payload():
    return get_token_payload(get_token_auth_header())


def get_token_header(header):
    """Obtains the Access Token from the a Header"""
    auth = request.headers.get(header, None)
    if not auth:
        raise Error(
            "unauthorized",
            "Authorization header is expected",
            traceback.format_stack(),
        )

    parts = auth.split()
    if parts[0].lower() != "bearer":
        raise Error(
            "unauthorized",
            "Authorization header must start with Bearer",
            traceback.format_stack(),
        )

    elif len(parts) == 1:
        raise Error("bad_request", "Token not found")
    elif len(parts) > 2:
        raise Error(
            "unauthorized",
            "Authorization header must be Bearer token",
            traceback.format_stack(),
        )

    return parts[1]  # Token


def get_token_auth_header():
    return get_token_header("Authorization")


def get_token_payload(token):
    # log.warning("The received token in get_token_payload is: " + str(token))
    try:
        claims = jwt.get_unverified_claims(token)
        secret = app.config["API_SECRET"]

    except:
        log.warning(
            "JWT token with invalid parameters. Can not parse it.: " + str(token)
        )
        raise Error(
            "unauthorized",
            "Unable to parse authentication parameters token.",
            traceback.format_stack(),
        )

    try:
        payload = jwt.decode(
            token,
            secret,
            algorithms=["HS256"],
            options=dict(verify_aud=False, verify_sub=False, verify_exp=True),
        )
    except jwt.ExpiredSignatureError:
        log.warning("Token expired")
        raise Error("unauthorized", "Token is expired", traceback.format_stack())

    except jwt.JWTClaimsError:
        raise Error(
            "unauthorized",
            "Incorrect claims, please check the audience and issuer",
            traceback.format_stack(),
        )
    except Exception:
        raise Error(
            "unauthorized",
            "Unable to parse authentication token.",
            traceback.format_stack(),
        )
    if payload.get("data", False):
        return payload["data"]
    return payload
