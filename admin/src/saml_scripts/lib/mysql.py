#!/usr/bin/env python
# coding=utf-8
import json
import logging as log
import time
import traceback
from datetime import datetime, timedelta

import mysql.connector
import yaml

# from admin import app


class Mysql:
    def __init__(self, host, database, user, password):
        self.conn = mysql.connector.connect(
            host=host, database=database, user=user, password=password
        )

    def select(self, sql):
        self.cur = self.conn.cursor()
        self.cur.execute(sql)
        data = self.cur.fetchall()
        self.cur.close()
        return data

    def update(self, sql):
        self.cur = self.conn.cursor()
        self.cur.execute(sql)
        self.conn.commit()
        self.cur.close()
