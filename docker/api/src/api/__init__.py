#!flask/bin/python
# coding=utf-8

import logging as log
import os

from flask import Flask, render_template, send_from_directory

app = Flask(__name__, static_url_path="")
app = Flask(__name__, template_folder="static/templates")
app.url_map.strict_slashes = False

"""
App secret key for encrypting cookies
You can generate one with:
    import os
    os.urandom(24)
And paste it here.
"""
app.secret_key = "Change this key!//\xf7\x83\xbe\x17\xfa\xa3zT\n\\]m\xa6\x8bF\xdd\r\xf7\x9e\x1d\x1f\x14'"

print("Starting isard-sso api...")

from api.lib.load_config import loadConfig

try:
    loadConfig(app)
except:
    print("Could not get environment variables...")


"""
Debug should be removed on production!
"""
if app.debug:
    log.warning("Debug mode: {}".format(app.debug))
else:
    log.info("Debug mode: {}".format(app.debug))

"""
Serve static files
"""


@app.route("/templates/<path:path>")
def send_templates(path):
    return send_from_directory(os.path.join(app.root_path, "static/templates"), path)


"""
Import all views
"""
from .views import AvatarsViews, InternalViews, MenuViews
