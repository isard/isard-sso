cp ../.env .
source .env
docker-compose stop isard-apps-nextcloud-app
docker rm isard-apps-nextcloud-app
rm -rf /opt/isard-office/nextcloud

echo "DROP DATABASE nextcloud;" | docker exec -i isard-apps-postgresql psql -U admin
docker-compose up -d isard-apps-nextcloud-app
docker-compose restart isard-apps-nextcloud-nginx
docker logs isard-apps-nextcloud-app --follow
