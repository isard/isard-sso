import random
import string
from collections import Counter
from pprint import pprint


def get_recursive_groups(l_groups, l):
    for d_group in l_groups:
        data = {}
        for key, value in d_group.items():
            if key == "subGroups":
                get_recursive_groups(value, l)
            else:
                data[key] = value
        l.append(data)
    return l


def get_group_with_childs(keycloak_group):
    return [g["path"] for g in get_recursive_groups([keycloak_group], [])]


def system_username(username):
    return (
        True
        if username in ["guest", "ddadmin", "admin"] or username.startswith("system_")
        else False
    )


def system_group(groupname):
    return True if groupname in ["admin", "manager", "teacher", "student"] else False


def get_group_from_group_id(group_id, groups):
    return next((d for d in groups if d.get("id") == group_id), None)


def get_kid_from_kpath(kpath, groups):
    ids = [g["id"] for g in groups if g["path"] == kpath]
    if not len(ids) or len(ids) > 1:
        return False
    return ids[0]


def get_gid_from_kgroup_id(kgroup_id, groups):
    return [
        g["path"].replace("/", ".")[1:] if len(g["path"].split("/")) else g["path"][1:]
        for g in groups
        if g["id"] == kgroup_id
    ][0]


def get_gids_from_kgroup_ids(kgroup_ids, groups):
    return [get_gid_from_kgroup_id(kgroup_id, groups) for kgroup_id in kgroup_ids]


def kpath2gid(path):
    # print(path.replace('/','.')[1:])
    if path.startswith("/"):
        return path.replace("/", ".")[1:]
    return path.replace("/", ".")


def kpath2gids(path):
    path = kpath2gid(path)
    l = []
    for i in range(len(path.split("."))):
        l.append(".".join(path.split(".")[: i + 1]))
    return l


def kpath2kpaths(path):
    l = []
    for i in range(len(path.split("/"))):
        l.append("/".join(path.split("/")[: i + 1]))
    return l[1:]


def gid2kpath(gid):
    return "/" + gid.replace(".", "/")


def count_repeated(itemslist):
    print(Counter(itemslist))


def groups_kname2gid(groups):
    return [name.replace(".", "/") for name in groups]


def groups_path2id(groups):
    return [g.replace("/", ".")[1:] for g in groups]


def groups_id2path(groups):
    return ["/" + g.replace(".", "/") for g in groups]


def filter_roles_list(role_list):
    client_roles = ["admin", "manager", "teacher", "student"]
    return [r for r in role_list if r in client_roles]


def filter_roles_listofdicts(role_listofdicts):
    client_roles = ["admin", "manager", "teacher", "student"]
    return [r for r in role_listofdicts if r["name"] in client_roles]


def rand_password(lenght):
    characters = string.ascii_letters + string.digits + string.punctuation
    passwd = "".join(random.choice(characters) for i in range(lenght))
    while not any(ele.isupper() for ele in passwd):
        passwd = "".join(random.choice(characters) for i in range(lenght))
    return passwd
