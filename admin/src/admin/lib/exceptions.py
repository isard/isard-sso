#!/usr/bin/env python
# coding=utf-8
class UserExists(Exception):
    pass


class UserNotFound(Exception):
    pass
