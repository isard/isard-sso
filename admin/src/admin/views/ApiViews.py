#!flask/bin/python
# coding=utf-8
import json
import logging as log
import os
import socket
import sys
import time
import traceback

from flask import request

from admin import app

from ..lib.api_exceptions import Error
from .decorators import has_token


## LISTS
@app.route("/ddapi/users", methods=["GET"])
@has_token
def ddapi_users():
    if request.method == "GET":
        sorted_users = sorted(app.admin.get_mix_users(), key=lambda k: k["username"])
        users = []
        for user in sorted_users:
            users.append(user_parser(user))
        return json.dumps(users), 200, {"Content-Type": "application/json"}


@app.route("/ddapi/users/filter", methods=["POST"])
@has_token
def ddapi_users_search():
    if request.method == "POST":
        data = request.get_json(force=True)
        if not data.get("text"):
            raise Error("bad_request", "Incorrect data requested.")
        users = app.admin.get_mix_users()
        result = [user_parser(user) for user in filter_users(users, data["text"])]
        sorted_result = sorted(result, key=lambda k: k["id"])
        return json.dumps(sorted_result), 200, {"Content-Type": "application/json"}


@app.route("/ddapi/groups", methods=["GET"])
@has_token
def ddapi_groups():
    if request.method == "GET":
        sorted_groups = sorted(app.admin.get_mix_groups(), key=lambda k: k["name"])
        groups = []
        for group in sorted_groups:
            groups.append(group_parser(group))
        return json.dumps(groups), 200, {"Content-Type": "application/json"}


@app.route("/ddapi/group/users", methods=["POST"])
@has_token
def ddapi_group_users():
    if request.method == "POST":
        data = request.get_json(force=True)
        sorted_users = sorted(app.admin.get_mix_users(), key=lambda k: k["username"])
        if data.get("id"):
            group_users = [
                user_parser(user)
                for user in sorted_users
                if data.get("id") in user["keycloak_groups"]
            ]
        elif data.get("path"):
            try:
                name = [
                    g["name"]
                    for g in app.admin.get_mix_groups()
                    if g["path"] == data.get("path")
                ][0]
                group_users = [
                    user_parser(user)
                    for user in sorted_users
                    if name in user["keycloak_groups"]
                ]
            except:
                raise Error("not_found", "Group path not found in system")
        elif data.get("keycloak_id"):
            try:
                name = [
                    g["name"]
                    for g in app.admin.get_mix_groups()
                    if g["id"] == data.get("keycloak_id")
                ][0]
                group_users = [
                    user_parser(user)
                    for user in sorted_users
                    if name in user["keycloak_groups"]
                ]
            except:
                raise Error("not_found", "Group keycloak_id not found in system")
        else:
            raise Error("bad_request", "Incorrect data requested.")
        return json.dumps(group_users), 200, {"Content-Type": "application/json"}


@app.route("/ddapi/roles", methods=["GET"])
@has_token
def ddapi_roles():
    if request.method == "GET":
        roles = []
        for role in sorted(app.admin.get_roles(), key=lambda k: k["name"]):
            log.error(role)
            roles.append(
                {
                    "keycloak_id": role["id"],
                    "id": role["name"],
                    "name": role["name"],
                    "description": role.get("description", ""),
                }
            )
        return json.dumps(roles), 200, {"Content-Type": "application/json"}


@app.route("/ddapi/role/users", methods=["POST"])
@has_token
def ddapi_role_users():
    if request.method == "POST":
        data = request.get_json(force=True)
        sorted_users = sorted(app.admin.get_mix_users(), key=lambda k: k["username"])
        if data.get("id", data.get("name")):
            role_users = [
                user_parser(user)
                for user in sorted_users
                if data.get("id", data.get("name")) in user["roles"]
            ]
        elif data.get("keycloak_id"):
            try:
                id = [
                    r["id"]
                    for r in app.admin.get_roles()
                    if r["id"] == data.get("keycloak_id")
                ][0]
                role_users = [
                    user_parser(user) for user in sorted_users if id in user["roles"]
                ]
            except:
                raise Error("not_found", "Role keycloak_id not found in system")
        else:
            raise Error("bad_request", "Incorrect data requested.")
        return json.dumps(role_users), 200, {"Content-Type": "application/json"}


## INDIVIDUAL ACTIONS
@app.route("/ddapi/user", methods=["POST"])
@app.route("/ddapi/user/<user_ddid>", methods=["PUT", "GET", "DELETE"])
@has_token
def ddapi_user(user_ddid=None):
    if request.method == "GET":
        user = app.admin.get_user_username(user_ddid)
        if not user:
            raise Error("not_found", "User id not found")
        return json.dumps(user_parser(user)), 200, {"Content-Type": "application/json"}
    if request.method == "DELETE":
        user = app.admin.get_user_username(user_ddid)
        if not user:
            raise Error("not_found", "User id not found")
        app.admin.delete_user(user["id"])
        return json.dumps({}), 200, {"Content-Type": "application/json"}
    if request.method == "POST":
        data = request.get_json(force=True)
        if not app.validators["user"].validate(data):
            raise Error(
                "bad_request",
                "Data validation for user failed: ",
                +str(app.validators["user"].errors),
                traceback.format_exc(),
            )

        if app.admin.get_user_username(data["username"]):
            raise Error("conflict", "User id already exists")
        data = app.validators["user"].normalized(data)
        keycloak_id = app.admin.add_user(data)
        if not keycloak_id:
            raise Error(
                "precondition_required",
                "Not all user groups already in system. Please create user groups before adding user.",
            )
        return (
            json.dumps({"keycloak_id": keycloak_id}),
            200,
            {"Content-Type": "application/json"},
        )

    if request.method == "PUT":
        user = app.admin.get_user_username(user_ddid)
        if not user:
            raise Error("not_found", "User id not found")
        data = request.get_json(force=True)
        if not app.validators["user_update"].validate(data):
            raise Error(
                "bad_request",
                "Data validation for user failed: "
                + str(app.validators["user_update"].errors),
                traceback.format_exc(),
            )
        data = {**user, **data}
        data = app.validators["user_update"].normalized(data)
        data = {**data, **{"username": user_ddid}}
        data["roles"] = [data.pop("role")]
        data["firstname"] = data.pop("first")
        data["lastname"] = data.pop("last")
        app.admin.user_update(data)
        if data.get("password"):
            app.admin.user_update_password(
                user["id"], data["password"], data["password_temporary"]
            )
        return json.dumps({}), 200, {"Content-Type": "application/json"}


@app.route("/ddapi/username/<old_user_ddid>/<new_user_did>", methods=["PUT"])
@has_token
def ddapi_username(old_user_ddid, new_user_did):
    user = app.admin.get_user_username(user_ddid)
    if not user:
        raise Error("not_found", "User id not found")
    # user = app.admin.update_user_username(old_user_ddid,new_user_did)
    return json.dumps("Not implemented yet!"), 419, {"Content-Type": "application/json"}


@app.route("/ddapi/group", methods=["POST"])
@app.route("/ddapi/group/<id>", methods=["GET", "POST", "DELETE"])
# @app.route("/api/group/<group_id>", methods=["PUT", "GET", "DELETE"])
@has_token
def ddapi_group(id=None):
    if request.method == "GET":
        group = app.admin.get_group_by_name(id)
        if not group:
            Error("not found", "Group id not found")
        return (
            json.dumps(group_parser(group)),
            200,
            {"Content-Type": "application/json"},
        )
    if request.method == "POST":
        data = request.get_json(force=True)
        if not app.validators["group"].validate(data):
            raise Error(
                "bad_request",
                "Data validation for group failed: "
                + str(app.validators["group"].errors),
                traceback.format_exc(),
            )
        data = app.validators["group"].normalized(data)
        data["parent"] = data["parent"] if data["parent"] != "" else None

        if app.admin.get_group_by_name(id):
            raise Error("conflict", "Group id already exists")

        path = app.admin.add_group(data)
        # log.error(path)
        # keycloak_id = app.admin.get_group_by_name(id)["id"]
        # log.error()
        return (
            json.dumps({"keycloak_id": None}),
            200,
            {"Content-Type": "application/json"},
        )
    if request.method == "DELETE":
        group = app.admin.get_group_by_name(id)
        if not group:
            raise Error("not_found", "Group id not found")
        app.admin.delete_group_by_id(group["id"])
        return json.dumps({}), 200, {"Content-Type": "application/json"}


@app.route("/ddapi/user_mail", methods=["POST"])
@app.route("/ddapi/user_mail/<id>", methods=["GET", "DELETE"])
@has_token
def ddapi_user_mail(id=None):
    if request.method == "GET":
        return (
            json.dumps("Not implemented yet"),
            200,
            {"Content-Type": "application/json"},
        )
    if request.method == "POST":
        data = request.get_json(force=True)

        # if not app.validators["mails"].validate(data):
        #     raise Error(
        #         "bad_request",
        #         "Data validation for mail failed: "
        #         + str(app.validators["mail"].errors),
        #         traceback.format_exc(),
        #     )
        for user in data:
            if not app.validators["mail"].validate(user):
                raise Error(
                    "bad_request",
                    "Data validation for mail failed: "
                    + str(app.validators["mail"].errors),
                    traceback.format_exc(),
                )
        for user in data:
            log.info("Added user email")
            app.admin.set_nextcloud_user_mail(user)
        return (
            json.dumps("Users emails updated"),
            200,
            {"Content-Type": "application/json"},
        )


def user_parser(user):
    return {
        "keycloak_id": user["id"],
        "id": user["username"],
        "username": user["username"],
        "enabled": user["enabled"],
        "first": user["first"],
        "last": user["last"],
        "role": user["roles"][0] if len(user["roles"]) else None,
        "email": user["email"],
        "groups": user.get("groups", user["keycloak_groups"]),
        "quota": user["quota"],
        "quota_used_bytes": user["quota_used_bytes"],
    }


def group_parser(group):
    return {
        "keycloak_id": group["id"],
        "id": group["name"],
        "name": group["name"].split(".")[-1],
        "path": group["path"],
        "description": group.get("description", ""),
    }


def filter_users(users, text):
    return [
        user
        for user in users
        if text in user["username"]
        or text in user["first"]
        or text in user["last"]
        or text in user["email"]
    ]
