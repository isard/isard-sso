#!/bin/sh
if [ ! -L /etc/letsencrypt/renewal-hooks/deploy/letsencrypt-hook-deploy-concatenante.sh ]
then
  mkdir -p /etc/letsencrypt/renewal-hooks/deploy/
  ln -s /usr/local/sbin/letsencrypt-hook-deploy-concatenante.sh /etc/letsencrypt/renewal-hooks/deploy/
fi

if [ -n "$LETSENCRYPT_DOMAIN" -a -n "$LETSENCRYPT_EMAIL" ]
then
  LETSENCRYPT_DOMAIN="$LETSENCRYPT_DOMAIN" crond
  if [ "$LETSENCRYPT_DOMAIN_ROOT" == "true" ]
  then
    option_root_domain="-d $LETSENCRYPT_DOMAIN"
  fi
  if [ ! -f /certs/chain.pem ]
  then
    if certbot certonly --standalone -m "$LETSENCRYPT_EMAIL" -n --agree-tos \
      -d "sso.$LETSENCRYPT_DOMAIN" \
      -d "api.$LETSENCRYPT_DOMAIN" \
      -d "admin.$LETSENCRYPT_DOMAIN" \
      -d "moodle.$LETSENCRYPT_DOMAIN" \
      -d "nextcloud.$LETSENCRYPT_DOMAIN" \
      -d "wp.$LETSENCRYPT_DOMAIN" \
      -d "oof.$LETSENCRYPT_DOMAIN" \
      -d "pad.$LETSENCRYPT_DOMAIN" \
      $option_root_domain
    then
      RENEWED_LINEAGE="/etc/letsencrypt/live/sso.$LETSENCRYPT_DOMAIN" /etc/letsencrypt/renewal-hooks/deploy/letsencrypt-hook-deploy-concatenante.sh
    fi
  fi
fi
