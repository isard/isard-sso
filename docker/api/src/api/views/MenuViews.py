#!flask/bin/python
# coding=utf-8
import json
import logging as log
import os
import sys
import time
import traceback
from uuid import uuid4

from api import app
from flask import Response, jsonify, redirect, render_template, request, url_for

from ..lib.menu import Menu

menu = Menu()


@app.route("/header/<format>", methods=["GET"])
@app.route("/header/<format>/<application>", methods=["GET"])
def api_v2_header(format, application=False):
    if application == False:
        if format == "json":
            if application == False:
                return (
                    json.dumps(menu.get_header()),
                    200,
                    {"Content-Type": "application/json"},
                )
        if format == "html":
            if application == False:
                return render_template("header.html")
            if application == "nextcloud":
                return render_template("header_nextcloud.html")
            if application == "wordpress":
                return render_template("header_wordpress.html")


# @app.route('/user_menu/<format>', methods=['GET'])
# @app.route('/user_menu/<format>/<application>', methods=['GET'])
# def api_v2_user_menu(format,application=False):
#     if application == False:
#         if format == 'json':
#             if application == False:
#                 return json.dumps(menu.get_user_nenu()), 200, {'Content-Type': 'application/json'}
